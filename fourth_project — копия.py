import pygame
import random

pygame.init()
pygame.sprite.Sprite
pygame.init()

WATER = "#0000aa"
GRASS = "#00aa00"
MOUNTAIN = "#555555"
FOREST = "#ffff55"
DIRT = "#880000"
WIGTH = 1300
HEIGHT = 1000
BWIGTH = 40
BHEIGHT = 40
PERWATER = 15
PERMOUNTAIN = 35
PERFOREST = 50
NUMOBLOCKWIGTH = WIGTH / BWIGTH
NUMOBLOCKHEIGHT = HEIGHT / BHEIGHT

class Scout(pygame.sprite.Sprite):
    
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.x = x
        self.y = y
        self.image = pygame.image.load("sYPSE-J9kso1.png")
        self.rect = pygame.Rect(x,y,40,40)
        self.select = False
        
    def click(self, key, pos):
        if key == 1:
            if self.point_in_object(pos) == True:
                self.select = True
            else:
                self.select = False
        
        elif key == 3:
            if self.select:
                self.warp(pos)
                
    def warp(self, pos):
        self.x = pos[0] / blockw * blockw
        self.y = pos[1] / blockh * blockh
        self.rect.x = pos[0] / blockw * blockw
        self.rect.y = pos[1] / blockh * blockh
        
    def update(self, delta_x, delta_y):
        self.x += delta_x
        self.y += delta_y
        self.rect.x += delta_x
        self.rect.y += delta_y
        
    def point_in_object(self, pos):
        if self.x <= pos[0] <= self.x + 35 and self.y <= pos[1] <= self.y + 35:
            return True
        else:
            return False
        
    def draw(self, screen):
        screen.blit(self.image, (self.rect.x + 5, self.rect.y + 5))
        
rand = 0
map_ = ([])
screen = pygame.display.set_mode((WIGTH, HEIGHT))
pygame.display.set_caption("civ")


for i in xrange(NUMOBLOCKWIGTH):
    map_.append([])
    
    for j in xrange(NUMOBLOCKHEIGHT):
        rand = random.randint(1, 100)
        surface = pygame.Surface((BWIGTH, BHEIGHT))
        color = GRASS
        
        if 0 < rand < PERWATER :
            color = WATER
            
        if PERWATER < rand < PERMOUNTAIN :
            color = MOUNTAIN
            
        if PERMOUNTAIN < rand < PERFOREST :
            color = FOREST

        surface.fill(pygame.Color(color))
        map_[i].append(surface)


bg = pygame.Surface((WIGTH, HEIGHT))
bg.fill(pygame.Color(DIRT))

scout = Scout(120, 120)



in_loop=True
while in_loop:
    for i in xrange(NUMOBLOCKWIGTH):
        for j in xrange(NUMOBLOCKHEIGHT):
            screen.blit(map_[i][j],(i * BWIGTH, j * BHEIGHT))
    delta_x = 0
    delta_y = 0
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.display.quit()
            in_loop=False
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            delta_x = 40
        if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            delta_x = -40
        if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            delta_y = 40
        if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            delta_y = -40
        elif event.type == pygame.MOUSEBUTTONDOWN:
            scout.click(event.button, event.pos)  
    
    scout.update(delta_x, delta_y)
    scout.draw(screen)
    pygame.display.update() 
    

