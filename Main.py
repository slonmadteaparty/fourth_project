def main_screen():
    for i in xrange(HEIGHT_RANGE):
        map_.append([])
        for j in xrange(WIDTH_RANGE):
            surface=pygame.Surface((POLE_WIDTH,POLE_HEIGHT))
            color="#00aa00"#dirt
            if random.randint(1,5)==2:
                color="#0000aa"#water
            if random.randint(1, 3) == 3:
                color = "#ffff30"#forest
            if random.randint(1, 20) == 7:
                color = "#444044"#mountains
            surface.fill(pygame.Color(color))
            map_[i].append(surface)
