from pygame import *
class Settler(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.x=x
        self.y=y
        self.image = Surface((44-4,44-4))
        self.image = image.load("unit-settler.png")
        self.rect = Rect(x,y,44,44)
    def update (self,delta_x,delta_y):
        self.x+=delta_x
        self.y+=delta_y
        self.rect.x+=delta_x
        self.rect.y+=delta_y
    def draw (self,screen):
        screen.blit(self.image,(self.rect.x+2,self.rect.y+2))
