import pygame
import random
from Settler import Settler
from Scout import Scout
pygame.init()


WIDTH = 1200
HEIGHT = 900
HEIGHT_RANGE = 44
WIDTH_RANGE = 44
POLE_WIDTH = WIDTH_RANGE
POLE_HEIGHT = HEIGHT_RANGE
screen=pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption("civ")
scout=[Scout(3 * POLE_WIDTH,3 * POLE_HEIGHT)]
settler = [Settler(3 * POLE_WIDTH, 3 * POLE_WIDTH)]
map_=[]
for i in xrange(HEIGHT_RANGE):
    map_.append([])
    for j in xrange(WIDTH_RANGE):
        surface=pygame.Surface((POLE_WIDTH,POLE_HEIGHT))
        color="#00aa00"#dirt
        if random.randint(1,5)==2:
            color="#0000aa"#water
        if random.randint(1, 3) == 3:
            color = "#ffff30"#forest
        if random.randint(1, 20) == 7:
            color = "#444044"#mountains
        surface.fill(pygame.Color(color))
        map_[i].append(surface)
   
bg = pygame.Surface((WIDTH,HEIGHT))
bo = False
goingscout = 0
goingsettler = 0
in_loop = True
while in_loop:
    delta_x = 0
    delta_y = 0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.display.quit()
            in_loop=False
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
            delta_x = POLE_WIDTH
        if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
            delta_x = -POLE_WIDTH
        if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
            delta_y = POLE_HEIGHT
        if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
            delta_y = -POLE_HEIGHT      
        
        if event.type == pygame.MOUSEBUTTONDOWN and bo:
            delta_x = (event.pos[0] - scout[goingscout].x) / POLE_WIDTH * POLE_WIDTH
            delta_y = (event.pos[1] - scout[goingscout].y) / POLE_HEIGHT * POLE_HEIGHT
            delta_x = (event.pos[0] - scout[goingsettler].x) / POLE_WIDTH * POLE_WIDTH
            delta_y = (event.pos[1] - scout[goingsettler].y) / POLE_HEIGHT * POLE_HEIGHT
            
            bo=False
        if event.type == pygame.MOUSEBUTTONDOWN:
            for i in xrange(len(scout)):
                if scout[i].x > event.pos[0] - POLE_WIDTH and scout[i].x < event.pos[0] + POLE_WIDTH:
                    if scout[i].y > event.pos[1] - POLE_HEIGHT and scout[i].y < event.pos[1] + POLE_HEIGHT:
                        bo=True
                        goingscout=i

            for i in xrange(len(settler)):
                if settler[i].x > event.pos[0] - POLE_WIDTH and settler[i].x < event.pos[0] + POLE_WIDTH:
                    if settler[i].y > event.pos[1] - POLE_HEIGHT and settler[i].y < event.pos[1] + POLE_HEIGHT:
                        bo=True
                        goingsettler = i
    
    for i in xrange(len(scout)):
        scout[i].update(delta_x,delta_y)
    for i in xrange(HEIGHT_RANGE):
        for j in xrange(WIDTH_RANGE):
            screen.blit(map_[i][j],(i * POLE_WIDTH,j * POLE_HEIGHT))
    for i in xrange(len(scout)):
        scout[i].draw(screen)

    for j in xrange(len(settler)):
        settler[j].update(delta_x, delta_y)
    for j in xrange(HEIGHT_RANGE):
        for q in xrange(WIDTH_RANGE):
            screen.blit(map_[j][q],(j * POLE_WIDTH,q * POLE_HEIGHT))
    for j in xrange(len(settler)):
        settler[j].draw(screen)
    pygame.display.update()

    

